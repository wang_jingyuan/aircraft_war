# -*- coding:utf-8 -*-
#导入模块
import pygame

from boss_bullet import Boss_Bullet       #boss子弹类
class Boss(pygame.sprite.Sprite):
    def __init__(self,boss,screen):
        pygame.sprite.Sprite.__init__(self)
        self.screen = screen
        self.boss = boss
        #获取窗口矩形
        self.screen_rect = screen.get_rect()
        #获取boos的矩形
        self.rect = boss.get_rect()
        #将每艘新飞船放在屏幕中央顶部
        self.rect.centerx = self.screen_rect.centerx
        self.rect.top = self.screen_rect.top + 35
        self.speed = [1,1]                                  # 初始化boos速度，这里是一个确定的值
        self.boss_bullets = pygame.sprite.Group()     # boss子弹的Group
        self.is_hit = False                             # 玩家是否被击中
    #boss移动
    def boos_moving(self):
        self.rect = self.rect.move(self.speed)
        if (self.rect.left < 0) or (self.rect.right > 480):
            self.speed[0] = -self.speed[0]
        if (self.rect.top < 24) or (self.rect.bottom > 540):
            self.speed[1] = -self.speed[1]

    #boss射击
    def boos_shoot(self,bullet_boss):
        boss_bullet_init_pos = self.rect.midbottom
        bullet_boss = Boss_Bullet(bullet_boss, boss_bullet_init_pos)
        self.boss_bullets.add(bullet_boss)
        
    def blit_me(self):
        '''在指定位置绘制boos'''
        self.screen.blit(self.boss,self.rect)
