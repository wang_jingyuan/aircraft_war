# -*- coding:utf-8 -*-
# 子弹（Bullet）类
# 此类继承自pygame.sprite.Sprite类
# 该类需要继承父类的属性，继承方法pygame.sprite.Sprite.__init__(self)
# 该类除了拥有从父类继承的属性外，还拥有四个自己的属性（实例变量），他们是：
# 1. 子弹图片（image），需要通过调用者传参赋值
# 2. 子弹运动的速度（speed），默认是8，且不变化
# 3. 子弹图片在窗口占据的矩形框（rect），通过self.image.get_rect()获得
# 4. 子弹图片在窗口占据的矩形框的底部中间的坐标（rect.topleft），需要通过调用者传参赋值
import pygame
class Bullet(pygame.sprite.Sprite):
    def __init__(self, bullet, bullet_init_pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = bullet
        self.rect = self.image.get_rect()
        self.rect.midbottom = bullet_init_pos
        self.speed = 8
    # 控制子弹移动
    def update(self):
        self.rect.top -= self.speed
        if self.rect.top < -self.rect.height:
            self.kill()
