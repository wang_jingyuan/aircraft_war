#游戏game 船ship 事件event 初始化init 移动 mobile
#屏幕screen 更新update 数据data 日期date
#开始start 检查check 位块传输blit 血blood 角色 role crash碰撞

#导入相关模块
import pygame
from sys import exit
from pygame.locals import *
from setting import Setting     #窗口配置类
from enemy import Enemy         #敌机类
from player import Player       #玩家类
from boss import Boss           #boss类
from prop import Prop           #道具类
import random
import stop as game_stop          #暂停函数
#导入时间模块
import time

#指定图像文件名称
background_image = 'image/classic_background.jpg' #经典背景图
blood_image = 'image/blood.png'                   #血条图
residual_blood_image = 'image/residual_blood.png' #残血图
yellow_bullet_image = 'image/yellow_bullet.png'   #黄子弹图
blue_bullet_image = 'image/blue_bullet.png'     #蓝子弹图
red_bullet_image = 'image/red_bullet.png'       #红子弹图
boss_bullet_image = 'image/boss_bullet.png'      #boss子弹图
enemy_plane_image = 'image/enemy_plane.png'       #敌机图
ship_2_image = 'image/ship_2.png'                     #玩家飞机
hp_image = 'image/hp.png'                     #玩家hp
time_image = 'image/time.png'                   #时间
boss_image = 'image/boss.png'                   #boss
boss_blood_image = 'image/boss_blood.png'       #BOsS血条
boss_hp_image = 'image/boss_hp.png'             #boss hp
stop_image = 'image/stop.png'                   #暂停按钮
#暂停模块图片
rmrematch_image = 'image/rmrematch.png'     #再来一局
back_main_image = 'image/back_main.png'     #返回主页面
continue_game_image = 'image/continue_game.png' #继续游戏

prop_image = 'image/prop.png'     #道具
    
def classic_model():
    #初始化一个窗口
    pygame.init()
    setting = Setting()
    screen = pygame.display.set_mode((setting.screen_width,setting.screen_height))
    pygame.display.set_caption(setting.title)
    
    
    #加载并转换图像
    background = pygame.image.load(background_image).convert()
    blood = pygame.image.load(blood_image).convert()
    residual_blood = pygame.image.load(residual_blood_image).convert()
    
    yellow_bullet = pygame.image.load(yellow_bullet_image)
    blue_bullet = pygame.image.load(blue_bullet_image)
    red_bullet = pygame.image.load(red_bullet_image)
    bullet_boss = pygame.image.load(boss_bullet_image)
    
    enemy_plane = pygame.image.load(enemy_plane_image)
    ship_2 = pygame.image.load(ship_2_image)
    hp = pygame.image.load(hp_image)
    time = pygame.image.load(time_image)
    boss = pygame.image.load(boss_image)
    boss_blood = pygame.image.load(boss_blood_image)
    boss_hp = pygame.image.load(boss_hp_image)
    stop = pygame.image.load(stop_image)
    #暂停模块图片
    rmrematch = pygame.image.load(rmrematch_image)
    back_main = pygame.image.load(back_main_image)
    continue_game = pygame.image.load(continue_game_image)

    prop = pygame.image.load(prop_image)
    #玩家数据初始化
    player = Player(ship_2,screen)
    #boos初始化
    boss = Boss(boss,screen)
##    enemy = Enemy(enemy_plane,,)    
    #游戏相关控制变量
    minus = 0 #减
    boss_minus = 0
    add = 0 #加
    shoot_frequency = 0  #控制子弹生成频率
    boss_shoot_frequency = 0 #控制boss子弹生成频率
    enemy_frequency = 0  #控制敌人生成频率
    prop_frequency = 0  #控制道具生成频率
    enemy_group = pygame.sprite.Group() #敌机组
    prop_group  = pygame.sprite.Group() #道具组
    enemy1_down_group = pygame.sprite.Group()   # 创建击毁敌人组
    boss_down_list = pygame.sprite.Group()
    boss_down_list_1 = pygame.sprite.Group() 
    total_time = 1 #总计时间
    
    #创建游戏时钟对象
    clock = pygame.time.Clock()
    #循环控制变量
    run = True
    #子弹
    bullet = yellow_bullet
    #子弹变换控制变量
    bullet_t = 0
    #开始游戏主循环
    while run:
        #设置屏幕刷新帧率  游戏最大帧率为60
        clock.tick(60)
        
        # 控制发射子弹频率,并发射子弹
        if not player.is_hit:
            if shoot_frequency % 15 == 0:
                #bullet_sound.play() #载入音乐
                player.single_shoot(bullet)
            shoot_frequency += 1
            if shoot_frequency >= 15:
                shoot_frequency = 0      
        # 生成敌机
        if enemy_frequency % 50 == 0:
            #创建敌机
            enemy = Enemy(enemy_plane,[random.randint(0, setting.screen_width - enemy_plane.get_width()), -enemy_plane.get_height()])
            enemy_group.add(enemy)
        enemy_frequency += 1
        if enemy_frequency >= 100:
            enemy_frequency = 0

        #道具
        if prop_frequency % 500 == 0:
            #实例化道具
            prop_1 = Prop(prop,[random.randint(0, setting.screen_width - prop.get_width()), -prop.get_height()])
            prop_group.add(prop_1)
        prop_frequency +=1
        if prop_frequency >= 1000:
            prop_frequency = 0
        # 判断玩家是否被击中
        enemy_down_list  = pygame.sprite.spritecollide(player,enemy_group,True)
        if len(enemy_down_list)>0:
            #enemy_down_group.add(enemy_down_list)
            minus+=1
##        if pygame.sprite.collide_rect(enemy, player):
##            player.is_hit = True
##            minus+=1
##            print(minus)
##            break
        # 检测敌机与子弹的碰撞
        enemy1_down_group.add(pygame.sprite.groupcollide(enemy_group, player.bullets, True, True)) 
        #检测玩家与道具的碰撞
        prop_down_list  = pygame.sprite.spritecollide(player,prop_group,True)
        if len(prop_down_list)>0:
            bullet_t += 1
            if bullet_t % 2 == 0:
                bullet = blue_bullet
            else:
                bullet = red_bullet
        if bullet_t >= 3:
            bullet_t = 0
            bullet = yellow_bullet
            
        #绘制背景
        screen.fill(0)
        screen.blit(background, (0, 0))
        #获取暂停位图的宽和高
        stop_width,stop_height = stop.get_size()
        #绘制暂停按钮
        stop_rect = screen.blit(stop,(240,0))
        #控制子弹
        player.bullets.update()
        # 绘制子弹
        player.bullets.draw(screen)
        # 控制敌机
        enemy_group.update()
        # 绘制敌机
        enemy_group.draw(screen)
        # 控制道具
        prop_group.update()
        # 绘制道具
        prop_group.draw(screen)
        
        
        #时间
        font = pygame.font.Font(None,25) #Nonepygame 自带字体
        taken_time = pygame.time.get_ticks() #ms 毫秒 得到以毫秒为间隔的时间
        life_time = total_time*60000 - taken_time #生存时间
        time_min = str(life_time//60000) #ls =1000ms 分钟
        time_sec = str(life_time%60000//1000)  #秒
        text_time = time_min.zfill(2)+':'+time_sec.zfill(2)
        timetext = font.render(text_time,True,(255,255,255))
        if taken_time >=total_time*60000:
            #绘制boss血条
            t = boss_blood.get_width()/residual_blood.get_width()
            health_m = t - boss_minus
            screen.blit(boss_hp,(300,0))
            screen.blit(boss_blood,(335,0))
            if health_m == 0:
                boss.is_hit = True
            else:
                for n in range(int(health_m)):
                    screen.blit(residual_blood,(335+n*residual_blood.get_width(),3))
            # 绘制boss
            if not boss.is_hit:
                #绘制boos
                boss.blit_me()
                # 控制boss发射子弹频率,并发射子弹
                if not boss.is_hit:
                    if boss_shoot_frequency % 30 == 0:
                        #bullet_sound.play() #载入音乐
                        boss.boos_shoot(bullet_boss)
                    boss_shoot_frequency += 1
                    if boss_shoot_frequency >= 30:
                        boss_shoot_frequency = 0  
                #boss移动
                boss.boos_moving()
                #控制子弹
                boss.boss_bullets.update()
                # 绘制boss子弹
                boss.boss_bullets.draw(screen)
                #pygame.display.update()
                #检测玩家子弹与boss的碰撞
                boss_down_list  = pygame.sprite.spritecollide(boss,player.bullets,True)
                if len(boss_down_list)>0:
                    boss_minus+=1
                #检测boss子弹与玩家碰撞
                boss_down_list_1  = pygame.sprite.spritecollide(player,boss.boss_bullets,True)
                if len(boss_down_list_1)>0:
                    minus+=1
                #检测boss与玩家碰撞
                crash = pygame.sprite.collide_rect(boss, player) #返回布尔值
                if crash == True:
                    minus+=1
            else:
                run = False 
##            print(life_time)
##            print(total_time*60000)
            
        else:
            #显示时间
            screen.blit(time,(415,0))
            screen.blit(timetext,(420,blood.get_height()/2+5))
        
        

        
        #将血条画上去
##        x = 35    #前面hp 血条的x坐标
##        screen.blit(blood,(0,0))
##        while x < blood.get_width() - residual_blood.get_width()-3:
##            x += 1
##            y = (x,3)
##            screen.blit(residual_blood,y)
        t = blood.get_width()/residual_blood.get_width()
        #vel_health = 4  *vel_health
        #health_m = heath_n - (minus-add)*vel_health
        health_m = t - minus
        screen.blit(hp,(0,3))
        screen.blit(blood,(35,0))
        if health_m == 0:
            player.is_hit = True
        else:
            for n in range(int(health_m)):
                screen.blit(residual_blood,(35+n*residual_blood.get_width()-3,3))
            
        # 绘制玩家飞机
        if not player.is_hit:
            player.blit_me()
        else:
##            font = pygame.font.Font(None,25) #Nonepygame 自带字体
##            timetext = font.render("玩家输了",True,(255,255,255))
            run = False 
            
##            player_down_index += 1
##            if player_down_index > 47:
##                run = False
       
        """检测用户的鼠标操作"""
        # 如果用户按下鼠标左键
        #if event.type == pygame.MOUSEMOTION:
##        if pygame.mouse.get_pressed()[0]:
##            # 获取鼠标坐标
##            pos = pygame.mouse.get_pos()
##            #如果用户点击"暂停"就跳到暂停界面
##            supply_timer = USEREVENT
##            if stop_rect.left < pos[0] < stop_rect.right and stop_rect.top < pos[1] < stop_rect.bottom:
##                #获取位图的宽和高
##                #再来一局按钮
##                rmrematch_width,rmrematch_height = rmrematch.get_size()
##                #返回主页面按钮
##                back_main_width,back_main_height = back_main.get_size()
##                #继续游戏按钮
##                continue_game_width,continue_game_height = continue_game.get_size()
##                #继续游戏按钮 continue_game_rect是图片“继续游戏”的上下左右的坐标
##                continue_game_rect = screen.blit(continue_game, ((setting.screen_width/2-continue_game_width/2),setting.screen_height/2))
##                #再来一局按钮 rmrematch_rect是图片“再来一局”的上下左右的坐标
##                rmrematch_rect = screen.blit(rmrematch, ((setting.screen_width/2-rmrematch_width/2),setting.screen_height/1.5))
##                #返回主页面按钮 back_main_rect是图片“返回主页面”的上下左右的坐标
##                back_main_rect = screen.blit(back_main, ((setting.screen_width/2-back_main_width/2),setting.screen_height/1.2))
##                """检测用户的鼠标操作"""
##                #如果用户按下鼠标左键
##                if pygame.mouse.get_pressed()[0]:
##                    # 获取鼠标坐标
##                    pos = pygame.mouse.get_pos()
##                    if continue_game_rect.left < pos[0] < continue_game_rect.right and continue_game_rect.top < pos[1] < continue_game_rect.bottom:
##                        pygame.quit()
##                        sys.exit()
##                        #让最近绘制的屏幕可见
##                        pygame.display.update()
##        else:          
        #让最近绘制的屏幕可见
        pygame.display.update()
            
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            
                
        # 监听键盘事件
        key_pressed = pygame.key.get_pressed()
        # 若玩家血条归零，则死亡
        if not player.is_hit:
            if key_pressed[K_w] or key_pressed[K_UP]:
                player.moveUp()
            if key_pressed[K_s] or key_pressed[K_DOWN]:
                player.moveDown()
            if key_pressed[K_a] or key_pressed[K_LEFT]:
                player.moveLeft()
            if key_pressed[K_d] or key_pressed[K_RIGHT]:
                player.moveRight()
        
                    
                




                
##while 1:
##    for event in pygame.event.get():
##        if event.type == pygame.QUIT:
##            pygame.quit()        
#classic_model()










