# -*- coding:utf-8 -*-
#游戏game 船ship 事件event 初始化init 移动 mobile
#屏幕screen 更新update 数据data 日期date
#开始start 检查check 位块传输blit 血blood 角色 role

#导入相关模块
import pygame
from sys import exit
from pygame.locals import *
from setting import Setting     #窗口配置类
from enemy import Enemy         #敌机类
from player import Player       #玩家类
from prop import Prop           #道具类
import random

#指定图像文件名称
background_image = 'image/integral_background.png' #积分背景图
life_image = 'image/life.png'                      #命
boss_blood_image = 'image/boss_blood.png'         #BOOS血条
#子弹
yellow_bullet_image = 'image/yellow_bullet.png'   #黄子弹图
blue_bullet_image = 'image/blue_bullet.png'     #蓝子弹图
red_bullet_image = 'image/red_bullet.png'       #红子弹图

enemy_plane_image = 'image/enemy_plane.png'       #敌机图
ship_2_image = 'image/ship_2.png'                     #玩家飞机
death_image = 'image/death.png'                    #死亡

integral_image = 'image/integral.png'         #积分图片

#暂停模块图片
rmrematch_image = 'image/rmrematch.png'     #再来一局
back_main_image = 'image/back_main.png'     #返回主页面
continue_game_image = 'image/continue_game.png' #继续游戏
#道具
prop_image = 'image/prop.png'     #道具


def integral_model():
    #初始化一个窗口
    pygame.init()
    setting = Setting()
    screen = pygame.display.set_mode((setting.screen_width,setting.screen_height))
    pygame.display.set_caption(setting.title)
    
    #加载并转换图像
    background = pygame.image.load(background_image).convert()
    life = pygame.image.load(life_image)
    
    yellow_bullet = pygame.image.load(yellow_bullet_image)
    blue_bullet = pygame.image.load(blue_bullet_image)
    red_bullet = pygame.image.load(red_bullet_image)

    integral = pygame.image.load(integral_image)
    enemy_plane = pygame.image.load(enemy_plane_image)
    ship_2 = pygame.image.load(ship_2_image)
    death = pygame.image.load(death_image)
    #道具
    prop = pygame.image.load(prop_image)
    #玩家数据初始化
    player = Player(ship_2,screen)
    #初始化
    
##    enemy = Enemy(enemy_plane,,)    
    #游戏相关控制变量
    minus = 0 #减
##    add = 0 #加
    shoot_frequency = 0  #控制子弹生成频率
    enemy_frequency = 0  #控制敌人生成频率
    prop_frequency = 0  #控制道具生成频率
    enemy_group = pygame.sprite.Group() #敌机组
    prop_group  = pygame.sprite.Group() #道具组
    enemy1_down_group = pygame.sprite.Group()   # 创建击毁敌人组
    life_always = 3     #玩家命的总数
    #创建游戏时钟对象
    clock = pygame.time.Clock()
    score = 0 #积分
    #循环控制变量
    run = True
    #子弹
    bullet = yellow_bullet
    #子弹变换控制变量
    bullet_t = 0
    #开始游戏主循环
    while run:
        #设置屏幕刷新帧率  游戏最大帧率为60
        clock.tick(60)
        
        # 控制发射子弹频率,并发射子弹  
        if not player.is_hit:
            if shoot_frequency % 15 == 0:
                #bullet_sound.play() #载入音乐
                player.single_shoot(bullet)
            shoot_frequency += 1
            if shoot_frequency >= 15:
                shoot_frequency = 0
                
        # 生成敌机
        if enemy_frequency % 30 == 0:
            #创建敌机
            enemy = Enemy(enemy_plane,[random.randint(0, setting.screen_width - enemy_plane.get_width()), -enemy_plane.get_height()])
            enemy_group.add(enemy)
        enemy_frequency += 1
        if enemy_frequency >= 30:
            enemy_frequency = 0


        #道具
        if prop_frequency % 500 == 0:
            #实例化道具
            prop_1 = Prop(prop,[random.randint(0, setting.screen_width - prop.get_width()), -prop.get_height()])
            prop_group.add(prop_1)
        prop_frequency +=1
        if prop_frequency >= 1000:
            prop_frequency = 0
            
        # 判断玩家是否被击中
        enemy_down_list  = pygame.sprite.spritecollide(player,enemy_group,True)
        if len(enemy_down_list)>0:
            #enemy1_down_group.add(enemy_down_list)
            minus+=1
        # 检测敌机与子弹的碰撞
        enemy1_down_group = pygame.sprite.groupcollide(enemy_group, player.bullets, True, True)   
        for enemy_down in enemy1_down_group:
             #enemy1_down_group.add(enemy_down)
             score += 1
             if score%100 == 0:
                life_always+=1
                
        #检测玩家与道具的碰撞
        prop_down_list  = pygame.sprite.spritecollide(player,prop_group,True)
        if len(prop_down_list)>0:
            bullet_t += 1
            if bullet_t % 2 == 0:
                bullet = blue_bullet
            else:
                bullet = red_bullet
        if bullet_t >= 3:
            bullet_t = 0
            bullet = yellow_bullet        


            
        #绘制背景
        screen.fill(0)
        screen.blit(background, (0, 0))
        #将命画上去  always 总 left剩
        y = 0
        t = 0
        while t<4:
            screen.blit(death,(y,0))
            y+=21
            t+=1
        x = 0 #每颗命之间的间隔
        life_left = life_always - minus
        
        if life_left == 0:
            player.is_hit = True
        else:
            if life_left >= 4:
                life_left = 4
                for n in range(life_left):
                    screen.blit(life,(x,0))
                    x+=21
            else:
                for n in range(life_left):
                    screen.blit(life,(x,0))
                    x+=21
            
        # 绘制玩家飞机
        if not player.is_hit:
            player.blit_me()
        else:
            run = False
##        # 绘制击毁动画
##        for enemy_down in enemy1_down_group:
##            #if enemy_down.down_index == 0:
##                #enemy1_down_sound.play()
##            if enemy_down.down_index > 7:
##               
##                continue
##            #screen.blit(enemy_down.down_imgs[enemy_down.down_index // 2], enemy_down.rect)
##            enemy_down.down_index += 1
        #控制子弹
        player.bullets.update()
        # 绘制子弹
        player.bullets.draw(screen)
        # 控制敌机
        enemy_group.update()
        # 绘制敌机
        enemy_group.draw(screen)
        # 控制道具
        prop_group.update()
        # 绘制道具
        prop_group.draw(screen)
        
       
       

##        # 绘制子弹和敌机
##        player.bullets.draw(screen)
##        enemies1.draw(screen)
        # 绘制得分
        score_font = pygame.font.Font(None, 36)
        score_text = score_font.render(str(score), True, (255, 255, 255))
        text_rect = score_text.get_rect()
        
        #获取位图的宽和高
        integral_width,integral_height = integral.get_size()
        score_text_width,score_text_height = score_text.get_size()
        text_rect.topleft = [400+integral_width/2-score_text_width/2, 15]
        screen.blit(integral,(400,0))
        screen.blit(score_text, text_rect)
        
        #让最近绘制的屏幕可见
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            
        # 监听键盘事件
        key_pressed = pygame.key.get_pressed()
        # 若玩家被击中，则无效
        if not player.is_hit:
            if key_pressed[K_w] or key_pressed[K_UP]:
                player.moveUp()
            if key_pressed[K_s] or key_pressed[K_DOWN]:
                player.moveDown()
            if key_pressed[K_a] or key_pressed[K_LEFT]:
                player.moveLeft()
            if key_pressed[K_d] or key_pressed[K_RIGHT]:
                player.moveRight()

##    font = pygame.font.Font(None, 48)
##    text = font.render('Score: '+ str(score), True, (255, 0, 0))
##    text_rect = text.get_rect()
##    text_rect.centerx = screen.get_rect().centerx
##    text_rect.centery = screen.get_rect().centery + 24
##    #screen.blit(game_over, (0, 0))
##    screen.blit(text, text_rect)

##    while 1:
##        for event in pygame.event.get():
##            if event.type == pygame.QUIT:
##                pygame.quit()
##        pygame.display.update()
#integral_model()
