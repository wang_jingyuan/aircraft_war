# -*- coding:utf-8 -*-
#右 right 左 left 前top  底部bottom 去 go后退 back 上 up 下来down 移动moving
#enemy_plane 敌机 single 单 shoot 射击 Group组
##convert_alpha可支持透明
##convert不支持
#导入pygame模块
import pygame
from bullet import Bullet       #子弹类
ship_2_image = 'image/ship_2.png'                     #玩家飞机
SCREEN_WIDTH = 480
SCREEN_HEIGHT = 640
# 玩家类
class Player(pygame.sprite.Sprite):
    def __init__(self, ship_2,screen):
        pygame.sprite.Sprite.__init__(self)
        self.screen = screen
        self.ship_2 = pygame.image.load(ship_2_image)
##        self.image = []                                 # 用来存储玩家对象精灵图片的列表
##        for i in range(len(player_rect)):
##            self.image.append(plane_img.subsurface(player_rect[i]).convert_alpha())
    
        #获取窗口矩形
        self.screen_rect = screen.get_rect()
        #获取飞船的矩形
        self.rect = ship_2.get_rect()
        #将每艘新飞船放在屏幕中央底部
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        
##        self.rect.topleft = init_pos                    # 初始化矩形的左上角坐标
        self.speed = 8                                  # 初始化玩家速度，这里是一个确定的值
        self.img_index = 0                              # 玩家精灵图片索引
        self.is_hit = False                             # 玩家是否被击中
        self.bullets = pygame.sprite.Group()     # 黄子弹的Group

    # 控制射击行为
    def single_shoot(self, bullet):
        bullet_init_pos = self.rect.midtop
        bullet = Bullet(bullet, bullet_init_pos)
        self.bullets.add(bullet)
    #向上移动
    def moveUp(self):
        if self.rect.top <= 0:
            self.rect.top = 0
        else:
            self.rect.top -= self.speed
    #向下移动
    def moveDown(self):
        if self.rect.top >= SCREEN_HEIGHT - self.rect.height:
            self.rect.top = SCREEN_HEIGHT - self.rect.height
        else:
            self.rect.top += self.speed
    #向左移动
    def moveLeft(self):
        if self.rect.left <= 0:
            self.rect.left = 0
        else:
            self.rect.left -= self.speed
    #向右移动
    def moveRight(self):
        if self.rect.left >= SCREEN_WIDTH - self.rect.width:
            self.rect.left = SCREEN_WIDTH - self.rect.width
        else:
            self.rect.left += self.speed

    def blit_me(self):
        '''在指定位置绘制飞船'''
        self.screen.blit(self.ship_2,self.rect)
