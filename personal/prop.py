# -*- coding:utf-8 -*-
import pygame
import random
class Prop(pygame.sprite.Sprite):
    def __init__(self , prop , prop_init_pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = prop
        self.rect = self.image.get_rect()
        self.rect.topleft = prop_init_pos
        self.speed = 5
##    def move(self):
##        if self.rect.top >= 640:        
##            self.reset()                         
##        else:                                    
##            self.rect.top += self.speed
    # 控制道具移动
    def update(self):
        self.rect.top += self.speed
        if self.rect.top > 640:
            self.kill()
