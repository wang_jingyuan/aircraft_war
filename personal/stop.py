# -*- coding:utf-8 -*-
import pygame
from setting import Setting     #窗口配置类
from sys import exit            #退出
#暂停模块图片
rmrematch_image = 'image/rmrematch.png'     #再来一局
back_main_image = 'image/back_main.png'     #返回主页面
continue_game_image = 'image/continue_game.png' #继续游戏
def game_stop(background):
    #初始化一个窗口
    pygame.init()
    setting = Setting()
    screen = pygame.display.set_mode((setting.screen_width,setting.screen_height))
    pygame.display.set_caption("暂停")
    #暂停模块图片
    rmrematch = pygame.image.load(rmrematch_image)
    back_main = pygame.image.load(back_main_image)
    continue_game = pygame.image.load(continue_game_image)
    #获取位图的宽和高
    #再来一局按钮
    rmrematch_width,rmrematch_height = rmrematch.get_size()
    #返回主页面按钮
    back_main_width,back_main_height = back_main.get_size()
    #继续游戏按钮
    continue_game_width,continue_game_height = continue_game.get_size()
    #继续游戏按钮 continue_game_rect是图片“继续游戏”的上下左右的坐标
    continue_game_rect = screen.blit(continue_game, ((setting.screen_width/2-continue_game_width/2),setting.screen_height/2))
    #再来一局按钮 rmrematch_rect是图片“再来一局”的上下左右的坐标
    rmrematch_rect = screen.blit(rmrematch, ((setting.screen_width/2-rmrematch_width/2),setting.screen_height/1.5))
    #返回主页面按钮 back_main_rect是图片“返回主页面”的上下左右的坐标
    back_main_rect = screen.blit(back_main, ((setting.screen_width/2-back_main_width/2),setting.screen_height/1.2))
    """检测用户的鼠标操作"""
    #如果用户按下鼠标左键
    if pygame.mouse.get_pressed()[0]:
        # 获取鼠标坐标
        pos = pygame.mouse.get_pos()
        if continue_game_rect.left < pos[0] < continue_game_rect.right and continue_game_rect.top < pos[1] < continue_game_rect.bottom:
            run = True
            return run
