# -*- coding:utf-8 -*-

#指定图像文件名称
background_image = 'image/background.jpg'
mouse_image = 'image/fugu.png'

#导入pygame库
import pygame

#导入配置类
from setting import Setting

#导入sys
import sys

def run_game():
    #初始化pygame,为使用硬件做准备
    pygame.init()
    setting = Setting()
    
    #创建了一个窗口
    screen = pygame.display.set_mode((setting.screen_width,setting.screen_height))
    
    #设置窗口标题
    pygame.display.set_caption(setting.title)

##    #创建一艘飞机
##    ship = Ship(screen)
    
    #加载并转换图像
    background = pygame.image.load(background_image).convert()
    mouse_cursor = pygame.image.load(mouse_image).convert_alpha()



##def showText(event):
##    print event.widget['text']
    #游戏主循环
    while True:
        #监视键盘和鼠标事件
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                #接收到退出事件后退出程序
                exit()
                
        #将背景图画上去
        screen.blit(background, (0,0))

        #获得鼠标位置
        x, y = pygame.mouse.get_pos()

        #计算光标的左上角位置
        x-= mouse_cursor.get_width() / 2
        y-= mouse_cursor.get_height() / 2
    
        #把光标画上去
        screen.blit(mouse_cursor, (x, y))
        
        #刷新一下画面
        pygame.display.update()
##        ship.blitme()


    

   
