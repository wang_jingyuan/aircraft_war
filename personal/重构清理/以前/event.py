# -*- coding:utf-8 -*-
#background背景 update 更新 death 死亡 health 健康 在这里满血 minus 减 add 加
#当按键按下的时候响应KEYDOWN事件，按键弹起的时候响应KEYDOWN事件
import pygame
from pygame.locals import *
#指定图像文件名称
background_image = 'image/classic_background.jpg'
blood_image = 'image/blood.png'
residual_blood_image = 'image/residual_blood.png'
boss_blood_image = 'image/boss_blood.png'
minus = 0 #减
add = 0 #加
def check_event(ship):
    #监视键盘和鼠标事件
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
                exit()
                
        elif event.type ==pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                ship.moving_right = True
            if event.key == pygame.K_LEFT:
                ship.moving_left = True
            if event.key == pygame.K_UP:
                ship.moving_up = True
            if event.key == pygame.K_DOWN:
                ship.moving_down = True
        elif event.type ==pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                ship.moving_right = False
            if event.key == pygame.K_LEFT:
                ship.moving_left = False
            if event.key == pygame.K_UP:
                ship.moving_up = False
            if event.key == pygame.K_DOWN:
                ship.moving_down = False 
    
    

def update_screen(screen,ship):
    #加载并转换图像
    background = pygame.image.load(background_image).convert()
    blood = pygame.image.load(blood_image).convert()
    residual_blood = pygame.image.load(residual_blood_image).convert()
    
    #绘制背景
    screen.fill(0)
    screen.blit(background,(0,0))
    #绘制飞船
    ship.blit_me()   
    #飞船移动
    ship.mobile(ship)
    #将血条画上去
    x = 35    #前面hp
    screen.blit(blood,(0,0))
    while x < blood.get_width() - residual_blood.get_width()-3:
        x += 1
        y = (x,3)
        screen.blit(residual_blood,y)
       
       
##    blood_health = residual_blood.get_width()/blood.get_width()
##    vel_health = 2
##    death = blood_health - (minus - add)*vel_health
##    for n in range(death):
##        screen.blit(residual_blood,(6+n*blood.get_width()-3,3))
##        if n > (residual_blood.get_width()-7):
##            break
    
    #让最近绘制的屏幕可见
    pygame.display.update()

