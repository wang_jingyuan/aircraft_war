# -*- coding:utf-8 -*-
#游戏game 船ship 事件event 初始化init 移动 mobile
#屏幕screen 更新update 数据data 日期date
#开始start 检查check 位块传输blit 血blood 角色 role

import pygame
from setting import Setting
from airship import Ship
import integral_model_event as event_init

def integral_model():
    #初始化游戏并创建一个屏幕对象
    #初始化一个窗口
    pygame.init()
    setting = Setting()
    screen = pygame.display.set_mode((setting.screen_width,setting.screen_height))
    pygame.display.set_caption(setting.title)
    #创建飞船
    ship = Ship(screen)
    
    #创建游戏时钟对象
    clock = pygame.time.Clock()
    
    #开始游戏主循环
    while True:
        #设置屏幕刷新帧率
        clock.tick(60)
        #监视键盘和鼠标事件
        event_init.check_event(ship)

        event_init.update_screen(screen,ship)    

#integral_model()
