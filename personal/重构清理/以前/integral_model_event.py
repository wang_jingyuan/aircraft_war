# -*- coding:utf-8 -*-
#background背景 update 更新 death 死亡 health 健康 在这里满血 minus 减 add 加
#当按键按下的时候响应KEYDOWN事件，按键弹起的时候响应KEYDOWN事件
import pygame
from pygame.locals import *
#指定图像文件名称
background_image = 'image/integral_background.png'
life_image = 'image/life.png'
minus = 0 #减
add = 0 #加
def check_event(ship):
    #监视键盘和鼠标事件
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
                exit()
                
        elif event.type ==pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                ship.moving_right = True
            if event.key == pygame.K_LEFT:
                ship.moving_left = True
            if event.key == pygame.K_UP:
                ship.moving_up = True
            if event.key == pygame.K_DOWN:
                ship.moving_down = True
        elif event.type ==pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                ship.moving_right = False
            if event.key == pygame.K_LEFT:
                ship.moving_left = False
            if event.key == pygame.K_UP:
                ship.moving_up = False
            if event.key == pygame.K_DOWN:
                ship.moving_down = False 
    
    

def update_screen(screen,ship):
    #加载并转换图像
    background = pygame.image.load(background_image).convert()
    life = pygame.image.load(life_image)
    
    #绘制背景
    screen.fill(0)
    screen.blit(background,(0,0))
    #绘制飞船
    ship.blit_me()   
    #飞船移动
    ship.mobile(ship)
    x = 0
    t = 0
    while t<3:
        screen.blit(life,(x,0))
        x+=21
        t+=1
    

    
    #让最近绘制的屏幕可见
    pygame.display.update()

