# -*- coding:utf-8 -*-
#右 right 左 left 前top  底部bottom 去 go后退 back 上 up 下来down 移动moving
##convert_alpha可支持透明 
##convert不支持
#导入pygame模块
import pygame
#指定图像文件名称 r'image\\ship.png'
airship_image = 'image/ship.png'

class Ship():
    def __init__(self,screen):
        self.screen = screen
        #加载并转换图像
        self.airshaip = pygame.image.load(airship_image)
        #获取飞船的矩形
        self.rect = self.airshaip.get_rect()
        #获取窗口矩形
        self.screen_rect = screen.get_rect()

        #将每艘新飞船放在屏幕中央底部
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False

    def blit_me(self):
        '''在指定位置绘制飞船'''
        self.screen.blit(self.airshaip,self.rect)

    def mobile(self,ship):
        speed = 8                      # 初始化玩家速度，这里是一个确定的值
        #右移
        if self.moving_right and self.rect.right<self.screen_rect.right:
            self.rect.centerx += speed
        #左移
        if self.moving_left and self.rect.left>self.screen_rect.left:
            self.rect.centerx -= speed
        #上移
        if self.moving_up and self.rect.top>self.screen_rect.top:
            self.rect.centery -= speed
        #下移
        if self.moving_down and self.rect.bottom<self.screen_rect.bottom:
            self.rect.centery += speed
        
