# -*- coding:utf-8 -*-
"""敌人类"""
#此类继承自pygame.sprite.Sprite类
# 该类需要继承父类的属性，继承方法pygame.sprite.Sprite.__init__(self)
# 该类除了拥有从父类继承的属性外，还拥有四个自己的属性（实例变量），他们是：
# 1. 敌人图片（image），需要通过调用者传参数
# 2. 敌人图片在窗口占据的矩形框（rect），通过self.image.get_rect()获得
# 3. 敌人图片在窗口占据的矩形框左上角（rect.topleft）的坐标，需要通过调用者传参数
     #暂时没有 4. 敌人被击杀的图片（down_imgs），需要通过调用者传参数
# 5. 敌人的移动速度（speed），默认是2，且不变化
# 6. 敌人被击杀的序号（down_index），初始值是0
import pygame
class Enemy(pygame.sprite.Sprite):
    def __init__(self, enemy_plane, enemy_init_pos):
        pygame.sprite.Sprite.__init__(self)
        self.image = enemy_plane
        self.rect = self.image.get_rect()
        self.rect.topleft = enemy_init_pos
        self.speed = 2
        self.down_index = 0
    def update(self):
        self.rect.top += self.speed
        #超出屏幕区域自动销毁对象
        if self.rect.top > 640:
            self.kill()
