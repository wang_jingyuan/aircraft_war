# -*- coding:utf-8 -*-
#指定图像文件名称
background_image = 'image/start.jpg'
classic_model = 'image/classic_model.jpg'
integral_model = 'image/integral_model.jpg'

#导入pygame库
import pygame

#导入一些常用的函数和常量
from pygame.locals import *

#导入
from main import run_game
#导入配置类
from setting import Setting
#导入main函数
import main as m

def run_start():
    #初始化pygame,为使用硬件做准备
    pygame.init()
    setting = Setting()
    
    #创建了一个窗口
    screen = pygame.display.set_mode((setting.screen_width,setting.screen_height))
    
    #设置窗口标题
    pygame.display.set_caption(setting.title)
    
    #加载并转换图像
    background = pygame.image.load(background_image).convert()
    classic = pygame.image.load(classic_model).convert()
    integral = pygame.image.load(integral_model).convert()


##def showText(event):
##    print event.widget['text']
    #游戏主循环
    while True:
        #监视键盘和鼠标事件
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                #接收到退出事件后退出程序
                exit()
                
        #将背景图画上去
        screen.blit(background, (0,0))

        #获取位图的宽和高
        #经典模式按钮
        classic_width,classic_height = classic.get_size()
        #积分模式按钮
        integral_width,integral_height = integral.get_size()
        
        #经典按钮 classic_rect是图片“经典模式”的上下左右的坐标
        classic_rect = screen.blit(classic, ((setting.screen_width/2-classic_width/2),setting.screen_height/1.5))
        #积分按钮 integral_rect是图片“积分模式”的上下左右的坐标
        integral_rect = screen.blit(integral, ((setting.screen_width/2-integral_width/2),setting.screen_height/1.2))

        """检测用户的鼠标操作"""
        # 如果用户按下鼠标左键
        #if event.type == pygame.MOUSEMOTION:
        if pygame.mouse.get_pressed()[0]:
            # 获取鼠标坐标
            pos = pygame.mouse.get_pos()
            #如果用户点击"经典模式"就开始经典玩法
            if classic_rect.left < pos[0] < classic_rect.right and classic_rect.top < pos[1] < classic_rect.bottom:
                #调用run_game开始经典玩法
                m.run_game()
            #如果用户点击"积分模式"就开始积分玩法
            elif integral_rect.left < pos[0] < integral_rect.right and integral_rect.top < pos[1] < integral_rect.bottom:
                pass
            
                      
        #刷新一下画面
        pygame.display.update()


run_start()

