# -*- coding:utf-8 -*-
#background背景 update 更新 death 死亡 health 健康 在这里满血 minus 减 add 加
#hit 打击
#当按键按下的时候响应KEYDOWN事件，按键弹起的时候响应KEYDOWN事件
import pygame
from pygame.locals import *
from sys import exit
import random
from enemy import Enemy

#指定图像文件名称
background_image = 'image/classic_background.jpg'
blood_image = 'image/blood.png'
residual_blood_image = 'image/residual_blood.png'
boss_blood_image = 'image/boss_blood.png'
yellow_bullet_image = 'image/yellow_bullet.png'
enemy_plane_image = 'image/enemy_plane.png'
minus = 0 #减
add = 0 #加

def check_event(ship):
    #监视键盘和鼠标事件
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
                exit()
                
        elif event.type ==pygame.KEYDOWN:
            if event.key == pygame.K_RIGHT:
                ship.moving_right = True
            if event.key == pygame.K_LEFT:
                ship.moving_left = True
            if event.key == pygame.K_UP:
                ship.moving_up = True
            if event.key == pygame.K_DOWN:
                ship.moving_down = True
        elif event.type ==pygame.KEYUP:
            if event.key == pygame.K_RIGHT:
                ship.moving_right = False
            if event.key == pygame.K_LEFT:
                ship.moving_left = False
            if event.key == pygame.K_UP:
                ship.moving_up = False
            if event.key == pygame.K_DOWN:
                ship.moving_down = False 
    
    

def update_screen(screen,ship,enemy_group):
##    #初始化一个飞机
##    ship = Ship(screen)
    shoot_frequency = 0  #控制子弹生成频率
    enemy_frequency = 0  #控制敌人生成频率
    #加载并转换图像
    background = pygame.image.load(background_image).convert()
    blood = pygame.image.load(blood_image).convert()
    residual_blood = pygame.image.load(residual_blood_image).convert()
    yellow_bullet = pygame.image.load(yellow_bullet_image)
    enemy_plane = pygame.image.load(enemy_plane_image)
    
    #绘制背景
    screen.fill(0)
    screen.blit(background,(0,0))
    #绘制飞船
    ship.blit_me()
    #飞船移动
    ship.mobile(ship)
    # 射击  ticks来控制射击频率
    if not ship.is_hit:
        if shoot_frequency % 15 == 0:
            #bullet_sound.play() #载入音乐
            ship.single_shoot(yellow_bullet)
        shoot_frequency += 1
        if shoot_frequency >= 15:
            shoot_frequency = 0

    # 生成敌机
    if enemy_frequency % 50 == 0:
        #创建敌机
        enemy = Enemy(enemy_plane,[randint(0, SCREEN_WIDTH - enemy_plane.get_width()), -enemy_plane.get_height()])
        enemy_group.add(enemy)
    enemy_frequency += 1
    if enemy_frequency >= 100:
        enemy_frequency = 0
    # 控制敌机
    enemy_group.update()
    # 绘制敌机
    enemy_group.draw(screen)
    #控制子弹
    ship.bullets_yellow.update()
    # 绘制子弹
    ship.bullets_yellow.draw(screen)
    #将血条画上去
    x = 35    #前面hp
    screen.blit(blood,(0,0))
    while x < blood.get_width() - residual_blood.get_width()-3:
        x += 1
        y = (x,3)
        screen.blit(residual_blood,y)

    
##    blood_health = residual_blood.get_width()/blood.get_width()
##    vel_health = 2
##    death = blood_health - (minus - add)*vel_health
##    for n in range(death):
##        screen.blit(residual_blood,(6+n*blood.get_width()-3,3))
##        if n > (residual_blood.get_width()-7):
##            break
    
    #让最近绘制的屏幕可见
    pygame.display.update()

